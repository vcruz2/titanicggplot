Untitled
================

``` r
library(dplyr)
```

    ## 
    ## Attaching package: 'dplyr'

    ## The following objects are masked from 'package:stats':
    ## 
    ##     filter, lag

    ## The following objects are masked from 'package:base':
    ## 
    ##     intersect, setdiff, setequal, union

``` r
library(ggplot2)
library(DataExplorer)
```

# Load Titanic data for analysis

``` r
titanic <- read.csv("titanic.csv", stringsAsFactors = FALSE)
head(titanic, 1)
```

    ##   PassengerId Survived Pclass                    Name  Sex Age SibSp Parch
    ## 1           1        0      3 Braund, Mr. Owen Harris male  22     1     0
    ##      Ticket Fare Cabin Embarked
    ## 1 A/5 21171 7.25              S

# Set up factors.

``` r
titanic$Pclass <- as.factor(titanic$Pclass)
titanic$Survived <- as.factor(titanic$Survived)
titanic$Sex <- as.factor(titanic$Sex)
titanic$Embarked <- as.factor(titanic$Embarked)
str(titanic)
```

    ## 'data.frame':    891 obs. of  12 variables:
    ##  $ PassengerId: int  1 2 3 4 5 6 7 8 9 10 ...
    ##  $ Survived   : Factor w/ 2 levels "0","1": 1 2 2 2 1 1 1 1 2 2 ...
    ##  $ Pclass     : Factor w/ 3 levels "1","2","3": 3 1 3 1 3 3 1 3 3 2 ...
    ##  $ Name       : chr  "Braund, Mr. Owen Harris" "Cumings, Mrs. John Bradley (Florence Briggs Thayer)" "Heikkinen, Miss. Laina" "Futrelle, Mrs. Jacques Heath (Lily May Peel)" ...
    ##  $ Sex        : Factor w/ 2 levels "female","male": 2 1 1 1 2 2 2 2 1 1 ...
    ##  $ Age        : num  22 38 26 35 35 NA 54 2 27 14 ...
    ##  $ SibSp      : int  1 1 0 1 0 0 0 3 0 1 ...
    ##  $ Parch      : int  0 0 0 0 0 0 0 1 2 0 ...
    ##  $ Ticket     : chr  "A/5 21171" "PC 17599" "STON/O2. 3101282" "113803" ...
    ##  $ Fare       : num  7.25 71.28 7.92 53.1 8.05 ...
    ##  $ Cabin      : chr  "" "C85" "" "C123" ...
    ##  $ Embarked   : Factor w/ 4 levels "","C","Q","S": 4 2 4 4 4 3 4 4 4 2 ...

## Cual es la tasa de supervivencia?

``` r
ggplot(titanic, aes(x = Survived)) + 
  geom_bar()
```

![](Titanic_files/figure-gfm/unnamed-chunk-4-1.png)<!-- -->

## Porcentajes

``` r
prop.table(table(titanic$Survived))
```

    ## 
    ##         0         1 
    ## 0.6161616 0.3838384

## Mejorando la grafica

``` r
ggplot(titanic, aes(x = Survived)) + 
  theme_bw() +
  geom_bar() +
  labs(y = "Passenger Count",
       title = "Titanic Survival Rates")
```

![](Titanic_files/figure-gfm/unnamed-chunk-6-1.png)<!-- -->

``` r
plot_boxplot(titanic, by="Sex")
```

    ## Warning: Removed 177 rows containing non-finite values (stat_boxplot).

![](Titanic_files/figure-gfm/unnamed-chunk-7-1.png)<!-- -->

``` r
plot_boxplot(titanic, by = "Survived")
```

    ## Warning: Removed 177 rows containing non-finite values (stat_boxplot).

![](Titanic_files/figure-gfm/unnamed-chunk-7-2.png)<!-- -->

``` r
plot_boxplot(titanic, by = "Embarked")
```

    ## Warning: Removed 177 rows containing non-finite values (stat_boxplot).

![](Titanic_files/figure-gfm/unnamed-chunk-7-3.png)<!-- -->
